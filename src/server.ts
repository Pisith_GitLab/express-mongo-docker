import express from 'express';
import bodyParser from "body-parser";
import connectDB from './config/database';

// import todo from './routes/api/todo';
import todo from './routes/todo';

// Connect to DB
connectDB();

// Express Setup & Configuration
const app = express();
app.set('port', process.env.PORT || 8080);
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))


app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'text/html')
    res.end('<h1>Application Up & Running</h1>')
})

app.use('/api/todo', todo);

const port = app.get('port');
const server = app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${port}`)
});

export default server;
