import { Document, Model, model, Schema } from "mongoose";

/**
 * Interface to model the Todo Schema for Typescript
 * @param content: string
 * @param description: string
 */

export interface ITodo extends Document {
    content: string;
    description: string;
}

const todoSchema: Schema = new Schema({
    content: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    }
});

const Todo: Model<ITodo> = model('Todo', todoSchema);

export default Todo;