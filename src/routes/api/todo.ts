/**
 * Unused
 * separate to controller and route
 */


// import { Router, Request, Response } from "express";
// import HttpStatusCodes from "http-status-codes";
// import { check, validationResult } from 'express-validator/check';

// import Todo, { ITodo } from '../../models/Todo';

// const router: Router = Router();

// // @route   GET api/todo
// // @desc    Get all todos
// // @access  Public
// router.get('/', async (req: Request, res: Response) => {
//     try {
//         const todos = await Todo.find();
//         res.status(HttpStatusCodes.OK).json(todos);
//     } catch (err) {
//         console.log(err.message);
//         res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error lah!!!');
//     }
// });

// // @route   POST api/todo
// // @desc    Create a todo
// // @access  Public
// router.post('/', 
//     [
//         check('content', 'Content is required').not().isEmpty()
//     ], 
//     async (req: Request, res: Response) => {
//         // Validation
//         const errors = validationResult(req);
//         if (!errors.isEmpty()) {
//             return res
//                 .status(HttpStatusCodes.BAD_REQUEST)
//                 .json({ errors: errors.array() });
//         }
//         const {content, description} = req.body;
//         const todoFields = {
//             content,
//             description
//         };
//         try {
//             const todo: ITodo | null = new Todo(todoFields);
//             const data: ITodo = await todo.save();
//             res.status(HttpStatusCodes.CREATED).json({data});
//         } catch (err) {
//             console.log(err.message);
//             res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error lah!!!');
//         }
//     }
// )

// // @route   PUT api/todo/:id
// // @desc    Update a todo
// // @access  Public
// router.put('/:id',
//     [
//         check('content', 'Content is required').not().isEmpty()
//     ],
//     async (req: Request, res: Response) => {
//         try {
//             const { params: {id}, body } = req;
//             const data: ITodo | null = await Todo.findByIdAndUpdate(
//                 {_id: id},
//                 body
//             );
//             res.status(data ? HttpStatusCodes.OK : HttpStatusCodes.NOT_FOUND).json({data});
//         } catch (err) {
//             console.log(err.message);
//             res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error lah!!!');
//         }
//     }
// )

// // @route   GET api/todo/:id
// // @desc    Get todo by id
// // @access  Public
// router.get('/:id', async (req: Request, res: Response) => {
//     try {
//         const { params: {id}} = req;
//         const data: ITodo | null = await Todo.findById({_id: id});
//         res.status(data ? HttpStatusCodes.OK : HttpStatusCodes.NOT_FOUND).json({data});
//     } catch (err) {
//         console.log(err.message);
//         res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error lah!!!');
//     }
// })

// // @route   DELETE api/todo/:id
// // @desc    Delete todo
// // @access  Public
// router.delete('/:id', async (req: Request, res: Response) => {
//     try {
//         const { params: {id} } = req;
//         const data: ITodo | null = await Todo.findOneAndRemove({_id: id});
//         res.status(data ? HttpStatusCodes.OK : HttpStatusCodes.NOT_FOUND).json({data});
//     } catch (err) {
//         console.log(err.message);
//         res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error lah!!!');
//     }
// })

// export default router;


// // @route   POST api/todo
// // @desc    Create a todo
// // @access  Public
// // router.post('/', 
// //     [
// //         check('content', 'Content is required').not().isEmpty()
// //     ], 
// //     async (req: Request, res: Response) => {
// //         // Validation
// //         const errors = validationResult(req);
// //         if (!errors.isEmpty()) {
// //             return res
// //                 .status(HttpStatusCodes.BAD_REQUEST)
// //                 .json({ errors: errors.array() });
// //         }

// //         const {content, description} = req.body;

// //         const todoFields = {
// //             content,
// //             description
// //         };

// //         let todo: ITodo = await Todo.findOne({ content: content});

// //         if (todo) {
// //             // update
// //             todo = await Todo.findOneAndUpdate(
// //                 {$set: todoFields},
// //                 {new: true}
// //             )
// //             return res.json(todo);
// //         } else {
// //             // create
// //             todo = new Todo(todoFields);

// //             await todo.save();

// //             res.status(HttpStatusCodes.CREATED).json(todo);
// //         }

// //         try {

// //         } catch (err) {
// //             console.log(err.message);
// //             res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error lah!!!');
// //         }
// //     }
// // )