import { Router } from 'express';
import { check } from 'express-validator/check';

import { getTodos, getTodo, addTodo, updateTodo, deleteTodo } from '../../controllers/todo'

const router: Router = Router();

router.get('/', getTodos);
router.get('/:id', getTodo);
router.post('/',
    [
        check('content', 'Content is required').not().isEmpty()
    ],
    addTodo
);
router.put('/:id',
    [
        check('content', 'Content is required').not().isEmpty()
    ],
    updateTodo
);
router.delete('/:id', deleteTodo);

export default router;


