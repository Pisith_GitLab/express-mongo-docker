import { connect } from 'mongoose';
import dataSource from './dataSource.json';

const config = {...dataSource.database};

const connectDB = async () => {
    try {
        const mongoURI: string = config.mongoURI;
        await connect (mongoURI);
        console.log('MongoDB Connected ...');
    } catch (err) {
        console.log(err.message);
        // Exit process with failure
        process.exit(1);
    }
}

export default connectDB;